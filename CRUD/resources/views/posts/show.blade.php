@extends('adminlte.master')

@section('content')
    <div class="mt-3 ml-3">
        <h4>{{ $casts->nama }}</h4>
        <p>{{ $casts->bio }}</p>
    </div>
@endsection