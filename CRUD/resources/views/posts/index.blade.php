@extends('adminlte.master')
@section('content')
<div class="card">
    <div class="card-header">
      <h3 class="card-title">List Cast</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
        <a href="/cast/create" class="btn btn-primary btn-sm mb-3">Tambah</button></a>
        <table class="table table-bordered">
            <thead>
            <tr>
                <th style="width: 10px">#</th>
                <th style="width: 45%">Nama</th>
                <th style="width: 55%">Bio</th>
                <th style="width: 10px">Umur</th>
                <th style="width: 25px">Actions</th>
            </tr>
            </thead>
            <tbody>
                @forelse($casts as $key => $cast)
                    <tr>
                        <td>{{$key + 1}}</td>
                        <td>{{$cast->nama}}</td>
                        <td>{{$cast->bio}}</td>
                        <td>{{$cast->umur}}</td>
                        <td style="display: flex">
                            <a href="/cast/{{$cast->id}}" class="btn btn-info btn-sm">show</a>
                            <a href="/cast/{{$cast->id}}/edit" class="btn btn-default btn-sm">edit</a>
                            <form action="/cast/{{$cast->id}}" method="post">
                                @csrf
                                @method('delete')
                                <input type="submit" value="delete" class="btn btn-danger btn-sm">
                            </form>
                        </td>
                    </tr>
                @empty
                <tr>
                    <td colspan="40" style="text-align: center">Tidak ada data</td>
                </tr>
                @endforelse
            </tbody>
        </table>
    </div>
  </div>
@endsection
    