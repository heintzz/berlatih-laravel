<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class CastController extends Controller
{

    public function create(){
        return view('posts.create');
    }

    public function store(Request $request){

        $request->validate([
            'nama' => 'required|unique:casts|max:255',
            'umur' => 'required|integer',
            'bio' => 'required'
        ],
        [
            'nama.required' => 'nama tidak boleh kosong',
            'umur.required' => 'umur tidak boleh kosong',
            'bio.required' => 'bio tidak boleh kosong',
        ]);

        DB::table('casts')->insert([
            'nama' => $request['nama'],
            'umur' => $request['umur'],
            'bio' => $request['bio']
        ]);
        return redirect('/cast');
    }

    public function index(){
        $casts = DB::table('casts')->get();
        // dd($casts);
        return view('posts.index', compact('casts'));
    }

    public function show($id){
        $casts = DB::table('casts')->where('id', $id)->first();
        // dd($casts);
        return view('posts.show', compact('casts'));
    }

    public function edit($id){
        $casts = DB::table('casts')->where('id', $id)->first();

        return view('posts.edit', compact('casts'));
    }

    public function update($id, Request $request){
        $request->validate([
            'nama' => 'required|unique:casts|max:255',
            'umur' => 'required|integer',
            'bio' => 'required'
        ],
        [
            'nama.required' => 'nama tidak boleh kosong',
            'umur.required' => 'umur tidak boleh kosong',
            'bio.required' => 'bio tidak boleh kosong',
        ]);

        $query = DB::table('casts')
                 ->where('id', $id)
                 ->update([
                     'nama' => $request['nama'],
                     'bio' => $request['bio'],
                     'umur' => $requet['umur']
                 ]);
        return redirect('/cast');
    }

    public function destroy($id){
        $query = DB::table('casts')->where('id', $id)->delete();
        return redirect('/cast');
    }
}

