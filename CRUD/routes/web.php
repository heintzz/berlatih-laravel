<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/cast/create', 'App\Http\Controllers\CastController@create');
Route::get('/cast', 'App\Http\Controllers\CastController@index');
Route::post('/cast', 'App\Http\Controllers\CastController@store');
Route::get('/cast/{cast_id}', 'App\Http\Controllers\CastController@show');
Route::get('/cast/{cast_id}/edit', 'App\Http\Controllers\CastController@edit');
Route::put('/cast/{cast_id}', 'App\Http\Controllers\CastController@update');
Route::delete('/cast/{cast_id}', 'App\Http\Controllers\CastController@destroy');

