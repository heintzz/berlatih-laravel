<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'App\Http\Controllers\HomeController@home');
Route::get('/register', 'App\Http\Controllers\AuthController@register');
Route::post('/welcome', 'App\Http\Controllers\AuthController@welcome');

// Route::get('/', function () {
//     return view('welcome');
// });

// Route::get('/test/{angka}', function ($angka) {
//     return view('test', ["angka" => $angka]);
// });

// Route::get('/home/{nama}', function ($nama) {
//     return "Halo $nama";
// });

// Route::get('/form', 'App\Http\Controllers\RegisterController@form');

// Route::get('/sapa', 'App\Http\Controllers\RegisterController@sapa');
// Route::post('/sapa', 'App\Http\Controllers\RegisterController@sapa_post');