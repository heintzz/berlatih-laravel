<!DOCTYPE html>
<html>
    <head>
        <title>Sign Up Form SanberBook</title>
    </head>
    <body>
        <h1>Buat Account Baru!</h1>
        <h3>Sign Up Form</h3>
        <form action="/welcome" method="POST">
            @csrf
            <!-- method get -> masuk url
                 method post -> ga masuk url -->

            <label>First Name: </label><br><br>
            <input type="text" name="first-name"><br><br>

            <label>Last Name: </label><br><br>
            <input type="text" name="last-name"><br><br>

            <label>Gender: </label><br><br>
            <input type="radio" name="gender" value="male"> Male <br>
            <input type="radio" name="gender" value="female"> Female <br>
            <input type="radio" name="gender" value="other"> Other <br><br>

            <label>Nationality: </label><br><br>
            <select name="Nationality">
                <option value="Indonesian">Indonesian</option>
                <option value="Singaporean">Singaporean</option>
                <option value="Malaysian">Malaysian</option>
                <option value="Australian">Australian</option>
            </select><br><br>

            <label>Language Spoken: </label><br><br>
            <input type="checkbox" value="Indonesia"> Bahasa Indonesia <br>
            <input type="checkbox" value="English"> English <br>
            <input type="checkbox" value="Other"> Other <br><br>

            <label>Bio: </label><br><br>
            <textarea cols="30" rows="10"></textarea><br>

            <input type="submit" value="Sign Up">
        </form>
    </body>
</html>